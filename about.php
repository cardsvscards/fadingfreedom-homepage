<!doctype html>
<html>
	<head>
		<title>
			Fading Freedom
		</title>
		<link href='http://fonts.googleapis.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
			<?php include("inc/masthead.php"); ?>

			<section>
			<p>
				A multiplayer card game inspired by Bang! and seeing colors.
			</p>
			</section>
	</body>
</html>
